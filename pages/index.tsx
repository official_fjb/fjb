import Head from "next/head";
import React, { useState, useRef, useEffect } from "react";
import { Disclosure } from "@headlessui/react";
import { PlusIcon, MinusIcon } from "@heroicons/react/outline";
import dynamic from "next/dynamic";
import Footer from "../components/Footer";
import parse from "html-react-parser";

// Work around to use the Solana Wallet Adapter with Next.js
// https://github.com/solana-labs/wallet-adapter/issues/30
const WalletProvider = dynamic(
  () => import("../components/ClientWalletProvider"),
  {
    ssr: false,
  }
);

const faqs = [
  {
    question: "WHEN IS THE ICO?",
    answer:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it t",
  },
  {
    question: "HOW DO I BUY SOME FJB TOKENS?",
    answer:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it t",
  },
  {
    question: "IS THERE A ROADMAP?",
    answer:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it t",
  },
];

export default function Home() {
  const [hide, setHide] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setHide(true);
    }, 500);
  }, []);

  return (
    <div>
      <Head>
        <title>FJB Token | The Worlds First Free Speech Token</title>
        <meta charSet="utf-8" />
        <link
          href="https://unpkg.com/tailwindcss@^2.0/dist/tailwind.min.css"
          rel="stylesheet"
        />
        <link
          rel="preload"
          href="/fonts/Spot/Spot-Normal.ttf"
          as="font"
          crossOrigin=""
        />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta
          name="description"
          content="Fuck Joe Biden (FJB) acts as symbol of how monetary policy should work."
        />
        <meta name="keywords" content="Solana, Blockchain" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@official_fjb" />
        <meta
          name="twitter:title"
          content="FJB Token | The Worlds First Free Speech Token"
        />
        <meta
          name="twitter:description"
          content="Fuck Joe Biden (FJB) acts as symbol of how monetary policy should work."
        />
        <meta name="twitter:creator" content="@official_fjb" />
        <meta
          name="twitter:image"
          content="http://OfficialFJB.com/fjb/j-b2.png"
        />
        <meta
          property="og:title"
          content="FJB Token | The Worlds First Free Speech Token"
        />
        <meta property="og:url" content="http://OfficialFJB.com" />
        <meta property="og:type" content="website" />
        <meta
          property="og:image"
          content="http://OfficialFJB.com/fjb/j-b2.png"
        />
        <meta
          property="og:description"
          content="Fuck Joe Biden (FJB) acts as symbol of how monetary policy should work."
        />
      </Head>

      <main className="relative overflow-hidden">
        <WalletProvider />
        {hide ? null : (
          <div className="inside px-8 md:px-0 pb-8">
            <h1 className="fjb-header">fjb</h1>
            <div className="welcome w-full flex flex-col md:flex-row text-white overflow-visible py-8 px-24 rounded-md">
              <img
                src="/fjb/logo.png"
                className="logo-colors w-80 h-auto rounded-full"
              />
              <div className="md:ml-24 relative">
                <h1 className="uppercase text-2xl letter-spacing-3 custom-bold pt-8 md:pt-0 text-center md:text-left">
                  Fuck Joe Biden Token
                </h1>
                <h2 className="uppercase text-4xl letter-spacing-3 custom-bold">
                  The worlds first free speech token
                </h2>
                <p className="reg-font pt-8">
                  Fuck Joe Biden (FJB) acts as symbol of how monetary policy
                  should work. While the FED makes the money printer go Brrrrr.
                  We offer a fixed supply of 10,000,000 tokens.
                </p>
                <p className="pt-3 text-xl custom-bold">
                  Limited supply. Automatic reflection. Deflationary burn.
                </p>
                <div className="main-mint-cont text-base text-gray-300 sm:text-xl lg:text-lg xl:text-xl">
                  <div className="mint-container">
                    <div>
                      <a
                        href="https://discord.gg/vhhs3t3xqK"
                        target="_blank"
                        className="discord-bubble m-auto px-10 rounded-md absolute bottom-0 left-20"
                      >
                        <svg
                          className="hid-small h-auto w-10 md:w-8 mr-5"
                          width="71"
                          height="55"
                          viewBox="0 0 71 55"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <g clipPath="url(#clip0)">
                            <path
                              d="M60.1045 4.8978C55.5792 2.8214 50.7265 1.2916 45.6527 0.41542C45.5603 0.39851 45.468 0.440769 45.4204 0.525289C44.7963 1.6353 44.105 3.0834 43.6209 4.2216C38.1637 3.4046 32.7345 3.4046 27.3892 4.2216C26.905 3.0581 26.1886 1.6353 25.5617 0.525289C25.5141 0.443589 25.4218 0.40133 25.3294 0.41542C20.2584 1.2888 15.4057 2.8186 10.8776 4.8978C10.8384 4.9147 10.8048 4.9429 10.7825 4.9795C1.57795 18.7309 -0.943561 32.1443 0.293408 45.3914C0.299005 45.4562 0.335386 45.5182 0.385761 45.5576C6.45866 50.0174 12.3413 52.7249 18.1147 54.5195C18.2071 54.5477 18.305 54.5139 18.3638 54.4378C19.7295 52.5728 20.9469 50.6063 21.9907 48.5383C22.0523 48.4172 21.9935 48.2735 21.8676 48.2256C19.9366 47.4931 18.0979 46.6 16.3292 45.5858C16.1893 45.5041 16.1781 45.304 16.3068 45.2082C16.679 44.9293 17.0513 44.6391 17.4067 44.3461C17.471 44.2926 17.5606 44.2813 17.6362 44.3151C29.2558 49.6202 41.8354 49.6202 53.3179 44.3151C53.3935 44.2785 53.4831 44.2898 53.5502 44.3433C53.9057 44.6363 54.2779 44.9293 54.6529 45.2082C54.7816 45.304 54.7732 45.5041 54.6333 45.5858C52.8646 46.6197 51.0259 47.4931 49.0921 48.2228C48.9662 48.2707 48.9102 48.4172 48.9718 48.5383C50.038 50.6034 51.2554 52.5699 52.5959 54.435C52.6519 54.5139 52.7526 54.5477 52.845 54.5195C58.6464 52.7249 64.529 50.0174 70.6019 45.5576C70.6551 45.5182 70.6887 45.459 70.6943 45.3942C72.1747 30.0791 68.2147 16.7757 60.1968 4.9823C60.1772 4.9429 60.1437 4.9147 60.1045 4.8978ZM23.7259 37.3253C20.2276 37.3253 17.3451 34.1136 17.3451 30.1693C17.3451 26.225 20.1717 23.0133 23.7259 23.0133C27.308 23.0133 30.1626 26.2532 30.1066 30.1693C30.1066 34.1136 27.28 37.3253 23.7259 37.3253ZM47.3178 37.3253C43.8196 37.3253 40.9371 34.1136 40.9371 30.1693C40.9371 26.225 43.7636 23.0133 47.3178 23.0133C50.9 23.0133 53.7545 26.2532 53.6986 30.1693C53.6986 34.1136 50.9 37.3253 47.3178 37.3253Z"
                              fill="#ffffff"
                            />
                          </g>
                          <defs>
                            <clipPath id="clip0">
                              <rect width="71" height="55" fill="white" />
                            </clipPath>
                          </defs>
                        </svg>
                        <b>Join the community on Discord</b>
                      </a>
                    </div>
                    {/* {!wallet ? (
                  <FJBCountdown
                    startDate={startDate}
                    setIsActive={setIsActive}
                    content={content}
                  />
                ) : isActive ? (
                  <div className="mt-4 text-base sm:text-xl lg:text-lg xl:text-xl letter-spacing3 text-center">
                    <p style={{ color: "#FFD051" }}>
                      Full release now available
                    </p>
                    <button
                      className="mint-button my-6"
                      disabled={isSoldOut || isMinting || !isActive}
                      onClick={onMint}
                      // variant="contained"
                    >
                      {isSoldOut ? (
                        "SOLD OUT"
                      ) : isActive ? (
                        isMinting ? (
                          <CircularProgress />
                        ) : (
                          "MINT FOR 0.05"
                        )
                      ) : (
                        // Shouldn't get here.
                        <FJBCountdown
                          startDate={startDate}
                          setIsActive={setIsActive}
                          content={content}
                        />
                      )}
                    </button>
                  </div>
                ) : (
                  <FJBCountdown
                    startDate={startDate}
                    setIsActive={setIsActive}
                    content={content}
                  />
                )} */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}

        <div className="p-8 md:px-36">
          <div className="inside ban">
            <p className="blah">10M Supply</p>
            <p className="blah">12% Reflection</p>
            <p className="blah">2% Burn</p>
          </div>
          {/* <button className="mint-button my-6 rounded-md">BUY FJB</button>
          <a className="ml-6 mr-6 custom-hov" href="#">
            FREEDOM SWAP
          </a>
          <a className="custom-hov" href="#">
            LIVE CHART
          </a> */}
        </div>

        <div className="fjb-section py-10 text-white relative">
          <div className="inside px-8 md:px-0">
            <div className="flex flex-col md:flex-row">
              <div className="w-full md:w-2/3 md:pr-20 leading-5 text-base">
                <h2 className="uppercase text-3xl letter-spacing-2">
                  limited supply
                </h2>
                <p className="pt-4 reg-font">
                  Ghoulies debuted as{" "}
                  <a
                    className="simple-link"
                    href="https://opensea.io/collection/stinkybinky-collection"
                    target="_blank"
                  >
                    22 1-of-1 hand-drawings
                  </a>{" "}
                  created by{" "}
                  <a
                    className="simple-link"
                    href="https://twitter.com/ghoulareyou"
                    target="_blank"
                  >
                    @ghoulareyou
                  </a>{" "}
                  on the Ethereum blockchain.
                </p>
                <br />
                <p className="reg-font">
                  Those originals inspired the Ghoulie Gang's generatively
                  unique Halloween season from over 200+ hand-drawn traits. All
                  Ghoulies are rare and some spawns are more uncommon than
                  others but each ghoulie has its own personality.
                </p>
              </div>
              <img className="w-7/12 rounded-md" src="/fjb/fred.jpg" />
            </div>
          </div>
        </div>

        <div className="open-source text-white mt-16 py-10 relative">
          <div className="inside px-8 md:px-0">
            <div className="flex flex-col md:flex-row">
              <img
                className="w-full md:w-4/12 mt-8 md:mt-0 rounded-md h-full"
                src="/fjb/did-that.png"
              />
              <div className="w-full md:w-2/3 md:pl-24 leading-5 text-base pt-12 md:pt-0">
                <h2 className="uppercase text-4xl letter-spacing-2">
                  Automatic Reflection
                </h2>
                <p className="pt-4 reg-font">
                  Ghoulies are stored as SPL tokens on the{" "}
                  <a
                    className="simple-link"
                    href="https://solana.com/"
                    target="_blank"
                  >
                    Solana blockchain
                  </a>{" "}
                  and on{" "}
                  <a
                    className="simple-link"
                    href="https://www.arweave.org/"
                    target="_blank"
                  >
                    Arweave
                  </a>
                  . We use{" "}
                  <a
                    className="simple-link"
                    href="https://www.quicknode.com/"
                    target="_blank"
                  >
                    QuickNode
                  </a>{" "}
                  as our dedicated RPC provider to scale and handle high amounts
                  of traffic without rate limits. We use{" "}
                  <a
                    className="simple-link"
                    href="https://github.com/metaplex-foundation/metaplex/blob/master/rust/nft-candy-machine/src/lib.rs"
                    target="_blank"
                  >
                    Candy Machine
                  </a>{" "}
                  for our smart contract. Candy Machine helps us and you to have
                  fair mints. This protects us from over collection and
                  pre-launch minting malfunctions.
                  <br />
                  <br />
                  <br />
                  <a
                    className="simple-link"
                    href="https://github.com/ghoul-keeper/ghoulies"
                    target="_blank"
                  >
                    GIT REPO
                  </a>
                  <br />
                  <a>CANDY MACHINE ID COMING SOON...</a>
                  <br />
                  <a
                    className="simple-link"
                    href="https://link.medium.com/pz674xxPrkb"
                    target="_blank"
                  >
                    The New Solana NFT Standard
                  </a>
                  <br />
                  <a
                    className="simple-link"
                    href="https://link.medium.com/QhfrNHojkkb"
                    target="_blank"
                  >
                    Why Provenance Isn’t Talked About on the Solana Blockchain
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="deflationary-burn py-10 text-white mt-16 relative">
          <div className="inside px-8 md:px-0">
            <div className="flex flex-col md:flex-row">
              <div className="w-full md:w-2/3 md:pr-20 leading-5 text-base">
                <h2 className="uppercase text-3xl letter-spacing-2">
                  deflationary burn
                </h2>
                <p className="pt-4 reg-font">
                  Ghoulies debuted as{" "}
                  <a
                    className="simple-link"
                    href="https://opensea.io/collection/stinkybinky-collection"
                    target="_blank"
                  >
                    22 1-of-1 hand-drawings
                  </a>{" "}
                  created by{" "}
                  <a
                    className="simple-link"
                    href="https://twitter.com/ghoulareyou"
                    target="_blank"
                  >
                    @ghoulareyou
                  </a>{" "}
                  on the Ethereum blockchain.
                </p>
                <br />
                <p className="reg-font">
                  Those originals inspired the Ghoulie Gang's generatively
                  unique Halloween season from over 200+ hand-drawn traits. All
                  Ghoulies are rare and some spawns are more uncommon than
                  others but each ghoulie has its own personality.
                </p>
              </div>
              <img className="w-6/12 rounded-md" src="/fjb/fred.jpg" />
            </div>
          </div>
        </div>

        <div className="blur-bg">
          <div className="max-w-7xl mx-auto py-12 px-8 md:px-4 sm:py-16 sm:px-6 lg:px-8">
            <div className="mx-auto">
              <h2 className="uppercase text-left text-3xl md:text-4xl text-white letter-spacing-2 adjust-font">
                Frequently Asked Questions
              </h2>
              <dl className="mt-6 space-y-3">
                {faqs.map((faq) => (
                  <Disclosure
                    as="div"
                    key={faq.question}
                    className="pt-6 faq-item"
                  >
                    {({ open }) => (
                      <>
                        <dt className="text-lg">
                          <Disclosure.Button className="text-left w-full flex justify-between items-start text-white">
                            <span className="text-white text-2xl letter-spacing-1a adjust-font-small">
                              {faq.question}
                            </span>
                            <span className="ml-6 h-7 flex items-center">
                              {open ? (
                                <MinusIcon
                                  className="h-6 w-6"
                                  aria-hidden="true"
                                />
                              ) : (
                                <PlusIcon
                                  className="h-6 w-6"
                                  aria-hidden="true"
                                />
                              )}
                            </span>
                          </Disclosure.Button>
                        </dt>
                        <Disclosure.Panel as="dd" className="mt-2 pr-12">
                          <p className="whitespace-pre-wrap reg-font text-white">
                            {parse(
                              faq.answer.replace(/\[.*?\]\(.*?\)/g, (text) => {
                                let [_fullmatch, name, link] =
                                  /\[(.*?)\]\((.*?)\)/g.exec(text);
                                return `<a class="anchor-here" target="_blank" href="${link}">${name}</a>`;
                              })
                            )}
                          </p>
                        </Disclosure.Panel>
                      </>
                    )}
                  </Disclosure>
                ))}
              </dl>
            </div>
          </div>
        </div>

        <Footer />
      </main>
    </div>
  );
}
