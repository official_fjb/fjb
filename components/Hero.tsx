// @ts-nocheck
import React, { FC } from "react";
import { useEffect, useState, useMemo } from "react";
import Countdown from "react-countdown";

import * as anchor from "@project-serum/anchor";

import { WalletMultiButton } from "@solana/wallet-adapter-react-ui";
import { useAnchorWallet } from "@solana/wallet-adapter-react";
import { LAMPORTS_PER_SOL } from "@solana/web3.js";
import { CircularProgress, Snackbar } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import Navigation from "../components/Navigation";

import {
  CandyMachine,
  awaitTransactionSignatureConfirmation,
  getCandyMachineState,
  mintOneToken,
  shortenAddress,
} from "./candy-machine";

export interface HomeProps {
  candyMachineId: anchor.web3.PublicKey;
  config: anchor.web3.PublicKey;
  connection: anchor.web3.Connection;
  startDate: number;
  treasury: anchor.web3.PublicKey;
  txTimeout: number;
}

const FJBCountdown = (props) => {
  let startDate = props.startDate;
  let setIsActive = props.setIsActive;
  let content = props.content;

  return (
    <div className="mt-4 text-base sm:text-xl lg:text-lg xl:text-xl letter-spacing3 text-center">
      <p style={{ color: "#FFD051" }}>Full release now available</p>
      <Countdown
        date={startDate.getTime()}
        onMount={({ completed }) => completed && setIsActive(true)}
        onComplete={() => setIsActive(true)}
        renderer={(props) =>
          rendererForCountdown({
            days: props.days,
            hours: props.hours,
            minutes: props.minutes,
            seconds: props.seconds,
            completed: props.completed,
            content: content,
          })
        }
      />
    </div>
  );
};

const rendererForCountdown = ({
  days,
  hours,
  minutes,
  seconds,
  completed,
  content,
}) => {
  if (completed) {
    return (
      <span className="flex my-2 text-center items-center w-full justify-center">
        <WalletMultiButton
          className="wal-banner"
          style={{
            textTransform: content == "Connect Wallet" ? "uppercase" : "none",
          }}
        >
          {content}
        </WalletMultiButton>
      </span>
    );
  } else {
    return (
      <div
        className="flex font-bold pb-3 sm:pb-5 justify-evenly mt-8 lg:text-3xl md:text-3xl sm:text-2xl"
        style={{ color: "#46ddeb" }}
      >
        <span className="text-center">
          {days} <br /> Days
        </span>
        <span className="text-center">
          {hours} <br /> Hours
        </span>
        <span className="text-center">
          {minutes} <br /> Minutes
        </span>
        <span className="text-center">
          {seconds} <br /> Seconds
        </span>
      </div>
    );
  }
};

interface AlertState {
  open: boolean;
  message: string;
  severity: "success" | "info" | "warning" | "error" | undefined;
}

const Hero = (props: HomeProps) => {
  const [balance, setBalance] = useState<number>();
  const [isActive, setIsActive] = useState(false); // true when countdown completes
  const [isSoldOut, setIsSoldOut] = useState(false); // true when items remaining is zero
  const [isMinting, setIsMinting] = useState(false); // true when user got to press MINT
  const [isOpen, setIsOpen] = useState(false);
  const [itemsAvailable, setItemsAvailable] = useState(0);
  const [itemsRedeemed, setItemsRedeemed] = useState(0);
  const [itemsRemaining, setItemsRemaining] = useState(0);

  const [alertState, setAlertState] = useState<AlertState>({
    open: false,
    message: "",
    severity: undefined,
  });

  const [startDate, setStartDate] = useState(
    new Date(props.startDate * 1000000)
  );
  const [candyMachine, setCandyMachine] = useState<CandyMachine>();

  let startDateInThePast = startDate <= new Date();

  const wallet = useAnchorWallet();

  const base58 = useMemo(() => wallet?.publicKey?.toBase58(), [wallet]);
  const content = !base58
    ? "Connect Wallet"
    : base58.slice(0, 4) + ".." + base58.slice(-4);

  const refreshCandyMachineState = () => {
    (async () => {
      if (!wallet) return;

      const {
        candyMachine,
        goLiveDate,
        itemsAvailable,
        itemsRemaining,
        itemsRedeemed,
      } = await getCandyMachineState(
        wallet as anchor.Wallet,
        props.candyMachineId,
        props.connection
      );

      setItemsAvailable(itemsAvailable);
      setItemsRemaining(itemsRemaining);
      setItemsRedeemed(itemsRedeemed);

      setIsSoldOut(itemsRemaining === 0);
      setStartDate(goLiveDate);
      setCandyMachine(candyMachine);
    })();
  };

  const onMint = async () => {
    try {
      setIsMinting(true);
      if (wallet && candyMachine?.program) {
        const mintTxId = await mintOneToken(
          candyMachine,
          props.config,
          wallet.publicKey,
          props.treasury
        );

        const status = await awaitTransactionSignatureConfirmation(
          mintTxId,
          props.txTimeout,
          props.connection,
          "singleGossip",
          false
        );

        if (!status?.err) {
          setAlertState({
            open: true,
            message: "Congratulations! Mint succeeded!",
            severity: "success",
          });
        } else {
          setAlertState({
            open: true,
            message: "Mint failed! Please try again!",
            severity: "error",
          });
        }
      }
    } catch (error: any) {
      let message = error.msg || "Minting failed! Please try again!";
      if (!error.msg) {
        if (error.message.indexOf("0x138")) {
        } else if (error.message.indexOf("0x137")) {
          message = `SOLD OUT!`;
        } else if (error.message.indexOf("0x135")) {
          message = `Insufficient funds to mint. Please fund your wallet.`;
        }
      } else {
        if (error.code === 311) {
          message = `SOLD OUT!`;
          setIsSoldOut(true);
        } else if (error.code === 312) {
          message = `Minting period hasn't started yet.`;
        }
      }

      setAlertState({
        open: true,
        message,
        severity: "error",
      });
    } finally {
      if (wallet) {
        const balance = await props.connection.getBalance(wallet.publicKey);
        setBalance(balance / LAMPORTS_PER_SOL);
      }
      setIsMinting(false);
      refreshCandyMachineState();
    }
  };

  useEffect(() => {
    (async () => {
      if (wallet) {
        const balance = await props.connection.getBalance(wallet.publicKey);
        setBalance(balance / LAMPORTS_PER_SOL);
      }
    })();
  }, [wallet, props.connection]);

  useEffect(refreshCandyMachineState, [
    wallet,
    props.candyMachineId,
    props.connection,
  ]);

  return (
    <div>
      <WalletMultiButton
        className="wal"
        style={{
          textTransform: content == "Connect Wallet" ? "uppercase" : "none",
        }}
      >
        {content}
      </WalletMultiButton>
      <a
        href="https://solana.com/"
        target="_blank"
        className="hover:opacity-60"
      >
        <img src="/solana.png" className="sol-log" />
      </a>
      <Navigation setIsOpen={setIsOpen} isOpen={isOpen} />
      <div className="hero">
        <div className="inside px-8 md:px-0 pb-8">
          <h1 className="fjb-header">fjb</h1>
          <div className="welcome w-full flex flex-col md:flex-row text-white overflow-visible py-8 px-24 rounded-md">
            <img
              src="/fjb/logo.png"
              className="logo-colors w-80 h-auto rounded-full"
            />
            <div className="md:ml-24 relative">
              <h1 className="uppercase text-2xl letter-spacing-3 custom-bold pt-8 md:pt-0 text-center md:text-left">
                Fuck Joe Biden Token
              </h1>
              <h2 className="uppercase text-xl letter-spacing-2 italic md:text-left text-center md:pt-0 pt-4">
                The worlds first free speech token
              </h2>
              <p className="reg-font pt-8">
                Fuck Joe Biden (FJB) acts as symbol of how monetary policy
                should work. While the FED makes the money printer go Brrrrr. We
                offer a fixed supply of 10,000,000 tokens.
              </p>
              <p className="pt-3 text-xl custom-bold">
                Limited supply. Automatic reflection. Deflationary burn.
              </p>
              <div className="main-mint-cont text-base text-gray-300 sm:text-xl lg:text-lg xl:text-xl">
                <div className="mint-container">
                  <div>
                    <a
                      href="https://discord.gg/vhhs3t3xqK"
                      target="_blank"
                      className="discord-bubble m-auto px-10 rounded-md md:absolute md:bottom-0 md:left-20 md:mt-0 mt-8"
                    >
                      <svg
                        className="h-auto w-10 md:w-8 mr-5 hid-small"
                        width="71"
                        height="55"
                        viewBox="0 0 71 55"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g clipPath="url(#clip0)">
                          <path
                            d="M60.1045 4.8978C55.5792 2.8214 50.7265 1.2916 45.6527 0.41542C45.5603 0.39851 45.468 0.440769 45.4204 0.525289C44.7963 1.6353 44.105 3.0834 43.6209 4.2216C38.1637 3.4046 32.7345 3.4046 27.3892 4.2216C26.905 3.0581 26.1886 1.6353 25.5617 0.525289C25.5141 0.443589 25.4218 0.40133 25.3294 0.41542C20.2584 1.2888 15.4057 2.8186 10.8776 4.8978C10.8384 4.9147 10.8048 4.9429 10.7825 4.9795C1.57795 18.7309 -0.943561 32.1443 0.293408 45.3914C0.299005 45.4562 0.335386 45.5182 0.385761 45.5576C6.45866 50.0174 12.3413 52.7249 18.1147 54.5195C18.2071 54.5477 18.305 54.5139 18.3638 54.4378C19.7295 52.5728 20.9469 50.6063 21.9907 48.5383C22.0523 48.4172 21.9935 48.2735 21.8676 48.2256C19.9366 47.4931 18.0979 46.6 16.3292 45.5858C16.1893 45.5041 16.1781 45.304 16.3068 45.2082C16.679 44.9293 17.0513 44.6391 17.4067 44.3461C17.471 44.2926 17.5606 44.2813 17.6362 44.3151C29.2558 49.6202 41.8354 49.6202 53.3179 44.3151C53.3935 44.2785 53.4831 44.2898 53.5502 44.3433C53.9057 44.6363 54.2779 44.9293 54.6529 45.2082C54.7816 45.304 54.7732 45.5041 54.6333 45.5858C52.8646 46.6197 51.0259 47.4931 49.0921 48.2228C48.9662 48.2707 48.9102 48.4172 48.9718 48.5383C50.038 50.6034 51.2554 52.5699 52.5959 54.435C52.6519 54.5139 52.7526 54.5477 52.845 54.5195C58.6464 52.7249 64.529 50.0174 70.6019 45.5576C70.6551 45.5182 70.6887 45.459 70.6943 45.3942C72.1747 30.0791 68.2147 16.7757 60.1968 4.9823C60.1772 4.9429 60.1437 4.9147 60.1045 4.8978ZM23.7259 37.3253C20.2276 37.3253 17.3451 34.1136 17.3451 30.1693C17.3451 26.225 20.1717 23.0133 23.7259 23.0133C27.308 23.0133 30.1626 26.2532 30.1066 30.1693C30.1066 34.1136 27.28 37.3253 23.7259 37.3253ZM47.3178 37.3253C43.8196 37.3253 40.9371 34.1136 40.9371 30.1693C40.9371 26.225 43.7636 23.0133 47.3178 23.0133C50.9 23.0133 53.7545 26.2532 53.6986 30.1693C53.6986 34.1136 50.9 37.3253 47.3178 37.3253Z"
                            fill="#ffffff"
                          />
                        </g>
                        <defs>
                          <clipPath id="clip0">
                            <rect width="71" height="55" fill="white" />
                          </clipPath>
                        </defs>
                      </svg>
                      <b>Join the community on Discord</b>
                    </a>
                  </div>
                  {/* {!wallet ? (
                    <FJBCountdown
                      startDate={startDate}
                      setIsActive={setIsActive}
                      content={content}
                    />
                  ) : isActive ? (
                    <div className="mt-4 text-base sm:text-xl lg:text-lg xl:text-xl letter-spacing3 text-center">
                      <p style={{ color: "#FFD051" }}>
                        Full release now available
                      </p>
                      <button
                        className="mint-button my-6"
                        disabled={isSoldOut || isMinting || !isActive}
                        onClick={onMint}
                        // variant="contained"
                      >
                        {isSoldOut ? (
                          "SOLD OUT"
                        ) : isActive ? (
                          isMinting ? (
                            <CircularProgress />
                          ) : (
                            "MINT FOR 0.05"
                          )
                        ) : (
                          // Shouldn't get here.
                          <FJBCountdown
                            startDate={startDate}
                            setIsActive={setIsActive}
                            content={content}
                          />
                        )}
                      </button>
                    </div>
                  ) : (
                    <FJBCountdown
                      startDate={startDate}
                      setIsActive={setIsActive}
                      content={content}
                    />
                  )} */}
                </div>

                <Snackbar
                  open={alertState.open}
                  autoHideDuration={6000}
                  onClose={() => setAlertState({ ...alertState, open: false })}
                >
                  <Alert
                    onClose={() =>
                      setAlertState({ ...alertState, open: false })
                    }
                    severity={alertState.severity}
                  >
                    {alertState.message}
                  </Alert>
                </Snackbar>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hero;
